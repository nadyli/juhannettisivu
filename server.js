const express = require('express')
const app = express()
const port = 3000;
const users = require('./users');
const bodyParser = require("body-parser");
const ip = require("ip");
var mysql_commands = require('./mysql_commands');
app.use(bodyParser.urlencoded({ extended: false }));

var htmlBackButton = "</br> <form> <input type='button' value='Takaisin' onclick='window.history.back()'> </form>"


//app.get('/', (req, res) => res.send("murve"));
app.get('/', (req, res) => res.sendFile('./index.html', {"root": __dirname}));

app.post('/adduser', (req, res) => {
    var response = mysql_commands.addNewUser(req.body.username);
    response.then(function(result) {
        res.send(result + htmlBackButton);
    }, function(err) {
        res.send("Error: " + err);
    })
});    

app.use('/users', users);
app.use('/users/removeall', (req, res) => {
    var response = mysql_commands.removeAllUsers();
    response.then(function(result) {
        res.send(result + htmlBackButton);
    }, function(err) {
        res.send("Error: " + err);
    });
});

function goBack() {
    console.log("testi");
}

// Kuunnellaan käyttäjiä
app.listen(port, () => console.log("Palvelin onnistuneesti päällä osoitteessa: " + ip.address() + ":" + port));