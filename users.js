const express = require('express');
const router = express.Router();
var db = require('./db');

router.get('/', function(req, res) {
    db.query('SELECT * FROM users', function(err, results, fields) {
        if (err) {
            res.send(JSON.stringify({"status": 500, "error": err, "reponse": null}));
        } else {
            res.send(JSON.stringify({"status": 200, "error": null, "response": results}) + "</br> <form> <input type='button' value='Takaisin' onclick='window.history.back()'> </form>");
        }        
    })
});

module.exports = router;