var db = require('./db');
var response;

module.exports =  {
    addNewUser(username) {
        var sql = "INSERT INTO users (name) VALUES ('" + username + "')";
        return new Promise((resolve, reject) => {
            var isTaken = isUsernameTaken(username);            
            isTaken.then(function(result) {
                if(result) {
                    response = "Käyttäjänimi " + username + " on jo varattu."
                    resolve(response);
                } else {
                    db.query(sql, function (err) {
                        if(err) {
                            response = "Käyttäjää ei onnistuttu lisäämään. Error: " + err;
                            resolve(response);
                        } else {
                            response = "Käyttäjä nimeltä " + username + " on lisätty onnistuneesti."
                            resolve(response);
                        }
                    });

                }
            })
        })
    },
    removeAllUsers(){
        var sql = "TRUNCATE TABLE users";
        return new Promise((resolve, reject) => {
            db.query(sql, (err) => {
                if(err) {
                    response = "Käyttäjiä ei onnistuttu poistamaan. Error: " + err;
                    resolve(response);
                } else {
                    response = "Käyttäjät poistettu onnistuneesti."
                    resolve(response);
                }
            })
        });
    }
}
 
function isUsernameTaken(username) {
    return new Promise(function(resolve, reject) {
        var taken = false;
        var sqlQuery = "SELECT * FROM users WHERE name = '" + username + "'";
        db.query(sqlQuery, function (err, result) {
            var numRows = result.length;
            if(numRows == 0) {            
                taken = false;
            } else {
                taken = true;
            }
            if(err) {
                reject(err);
            } else {
                resolve(taken);
            }
        });
    });
}