// Singleton Database
var mysql = require('mysql');
var settings = require('./dbsettings.json');
const environment  = process.env.NODE_ENV || 'developement';

var db;

function connectToDatabase(){
    if(!db) {
        db = mysql.createConnection(settings[environment]);

        db.connect(function(err) {
            if(!err) {
                console.log('Tietokantayhteys muodostettu');
            } else {
                console.log('Ongelma tietokantayhteyden muodostamisessa: ' + err);
            }
        });
    }
    return db;
}

module.exports = connectToDatabase();